#!/bin/sh

find -type f -iname "*.pdf" | \
while read pdf_file_name
do
	if ! pdfinfo "$pdf_file_name" | grep -q '^Encrypted:\s*no'
	then
		echo "Warning: Please remove the encryption from $pdf_file_name before archiving it."
		exit 1
	fi
done
